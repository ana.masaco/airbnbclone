import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import Airbnb from './Components/Routes/AirbnbPage';
import MainPage from './Components/Routes/MainPage';


function Routes() {
  return (
    <BrowserRouter>
    <MainPage/>
    <Airbnb/>
    </BrowserRouter>
  );
}

export default Routes;