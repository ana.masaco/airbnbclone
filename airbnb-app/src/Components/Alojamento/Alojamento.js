import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import Card from "react-bootstrap/Card";
import { FaStar } from 'react-icons/fa';
import NavBar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';
import './Alojamento.css';

function Alojamento(){
    const [houses, setHouses] = useState([]);
    useEffect(()=>{
        Axios.get(`data.json`).then((res)=>{
            setHouses(res.data)
        }).catch((err) =>{ console.log(err)})
    },[])
    return(
        <div>
            <NavBar/>
            <div className="cardCointainer">
                <div className="cardTest">
                    {houses.map((item, index)=>(
                        <Card className="card" item key={index}>
                        <div className="cardBodyGeneralDiv">
                            <Card.Img className="cardImg" variant="top" src={item.img} alt="house" width="250" height="250"/>
                                <Card.Body className="cardBody">
                                    <Card.Title className="title">{item.name}</Card.Title>
                                    <div className="flexCard">
                                        <div className="city-country">
                                            <p className="city">{item.city}</p>
                                        </div>
                                        <div>
                                            <p className="country">{item.country}</p>
                                        </div>
                                    </div>
                                    <div className="flexCard">
                                        <div className="review-price">
                                            <p className="reviews"> <FaStar/> {item.reviews}</p>
                                        </div>
                                        <div>
                                            <p className="price">{item.price}€</p>
                                        </div>
                                    </div>
                                </Card.Body>
                            </div>
                            
                        </Card> 
                    ))}
                </div>
            </div>
            <Footer/>
        </div>
    )
}

export default Alojamento;