import React from 'react';
import { Link } from 'react-router-dom';

import LoginSideBar from '../Login/LoginSideBar';
import { Container, Row, Col} from 'react-bootstrap';
import { FiArrowLeft } from 'react-icons/fi';
import "./styles/SignUp.css";



export default function SignUp() {
  return (
<Container fluid className="containerLogin" id="page-restrict">
      <Row className="contentWrapper">
        <Col lg={6} className="registerCardDiv">
        <div className="registerCard">
          <div className="registerArea">
            <form className="registerForm">
                <h1>Dados</h1>
                <div className="inputArea">
                <div className="inputBlock">
                    <label htmlFor="user" className="registerlabels">Username:</label>
                    <input
                    className="registeruser"
                      id="user"
                      type="text"
                    />
                  </div>
                  <div className="inputBlock">
                    <label htmlFor="email" className="registerlabels">E-mail:</label>
                    <input
                    className="registeremail"
                      id="email"
                      type="email"
                    />
                  </div>
                  <div className="inputBlock">
                    <label htmlFor="password" className="registerlabels">Password:</label>
                    <input
                    className="registerpassword"
                      id="password"
                      type="password"
                    />
                  </div>
                </div>
              <button type="submit" className="registerButton">
                Confirmar
              </button>
            </form>
          </div>
          <Link to="/" className="registerBack">
            <FiArrowLeft size={35}/>
          </Link>
          </div>
          </Col>
          <Col lg={6} className="registerSideBarDiv">
          <LoginSideBar className="registerSideBar"/>
          </Col>
    </Row>
  </Container>
  );
}