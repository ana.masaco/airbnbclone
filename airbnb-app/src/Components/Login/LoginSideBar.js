import React from 'react';
import logo from '../img/AirBnBClone-logo.png';
import './LoginSideBar.css';

export default function LoginSideBar() {
  return (
    <aside className="loginSideBar">
      <img src={logo} alt="airbnb logo" />
    </aside>
  );
}