import React from 'react';
import './Footer.css';
import { Link } from 'react-router-dom';

export default function Footer() {
    return (
        <div className="footerContainer">
            <div className="footerBtn">
                <button>Community</button>
                <button>Help</button>
                <button>Support</button>
            </div>
            <div className="footerText">
                <p>© 2021 Airbnb, Inc.</p>
                <div className="footerTerms">
                    <button>Privacy</button>
                    <button>Terms</button>
                    <button>Sitemap</button>
                </div>
            </div>
        </div>
    );
}