import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { FiUser } from "react-icons/fi";
import './NavBar.css';


export default function NavBar() {
    return (
        <Navbar bg="light"  expand="lg" className="NavbarDiv">
            <Container fluid>
            <div className="NavbarClass">
                <div className="navLogo">
                    <Link to="/">
                        <h1 className="navLogoTitle">AirBnB 
                        <span className="navLogoSpan"> Clone</span></h1>
                    </Link>
                </div>
                <Navbar.Collapse className="navCollapse" id="navbarScroll">
                    <Nav
                        className="me-auto my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    ></Nav>
                    <div className="ourNavLinkDiv">
                        <Nav.Link className="ourNavLink ourNavLinkLogin" href="/login">Login</Nav.Link>
                        <Nav.Link className="ourNavLink" href="/signup">Sign Up</Nav.Link>
                    </div>
                    <div className="navUserIcon">
                        <div className="userIconDiv">
                            <Nav.Link href="/user">
                                <FiUser className="userIcon" size={32}/>
                            </Nav.Link>
                        </div>
                    </div>
                </Navbar.Collapse>
            </div>
            </Container>
        </Navbar>
    );
}
  