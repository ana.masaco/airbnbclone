import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col} from 'react-bootstrap';
import { FiArrowLeft } from 'react-icons/fi';
import LoginSideBar from '../Login/LoginSideBar';
import './styles/RestrictLogin.css';

export default function Restrict() {
  return (
    <Container fluid className="containerLogin" id="page-restrict">
      <Row className="contentWrapper">
        <Col lg={6} className="loginSideBarDiv">
          <LoginSideBar className="loginSideBar" />
        </Col>
        <Col lg={6} className="loginCardDiv">
          <div className="loginCard">
            <div className="login-area">
              <form className="login-form">
              <div>
                <Link to="/" className="loginArrowBack">
                  <FiArrowLeft size={35}/>
                </Link>
                <h1>Login</h1>
              </div>
                <div className="input-area">
                  <div className="input-block emailLoginDiv">
                    <label htmlFor="email" className="loginLabels">E-mail</label>
                    <input
                      className="loginInputs loginEmailInput"
                      id="email"
                      type="email"
                    />
                  </div>
                  <div className="input-block passLoginDiv">
                    <label htmlFor="password" className="loginLabels">Password</label>
                    <input
                        className="loginInputs"
                        id="password"
                        type="password"
                      />
                  </div>
                  <div className="help-area">
                    <Link to="/login/forgot" className="forgotPassword">
                      Forgot your Password?
                    </Link>
                    <div className="checkboxBlock">
                      <input type="checkbox" name="remember" id="remember" />
                      <label htmlFor="remember">Remember me</label>
                    </div>
                  </div>
                </div>
                <button type="submit" className="loginButton">
                  Login
                </button>
              </form>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}