import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Restrict from '../views/RestrictLogin';
 import ForgotLogin from '../views/ForgotLogin';

import Sign from "../views/SignUp";

export default function LoginRoutes() {
  return (
    <Switch>
      <Route exact path="/login" component={Restrict}></Route>
       <Route path="/login/forgot" component={ForgotLogin}></Route>

      <Route exact path="/signup" component={Sign}></Route>
    </Switch>
  );
}