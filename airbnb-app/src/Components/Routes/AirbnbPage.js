import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from '../Homepage/HomePage';
import Profile from '../Users/Users';
import Alojamento from '../Alojamento/Alojamento';


export default function AirbnbPage() {
  return (
    <Switch>
      <Route path="/" exact component={Home}></Route>
      <Route path="/user" component={Profile}></Route>
      <Route path="/alojamento" component={Alojamento}></Route>
    </Switch>
  );
}