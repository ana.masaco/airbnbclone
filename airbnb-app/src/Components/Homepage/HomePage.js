import React from 'react';
import { Nav } from 'react-bootstrap';
import video1 from '../img/video/1-search.mp4';
import video2 from '../img/video/2-go.mp4';
import video3 from '../img/video/3-go.mp4';
import NavBar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';
import './HomePage.css';

function HomePage() {
  return (
    <div id="page-landing">
      <div className="content-wrapper">
        <NavBar/>

        <header className="imgHeaderBack">
          <div class="wantStartBtnDiv">
            <Nav.Link className="wantStartBtn" href="/alojamento">I want to start!</Nav.Link>
          </div>
        </header>

        <main className="mainDiv">
          <div className="mainSecondaryDiv">
            <div className="mainTitle">
              <h1>You’re just 3 steps away from your next getaway</h1>
            </div>
            <div className="step1">
              <div className="step1Text">
                <h2>1. Browse</h2>
                <p>Start by exploring Stays or Experiences. Apply filters like entire homes, self check-in, or pets allowed to narrow your
                options. You can also save favorites to a wishlist.</p>
              </div>
              <div className="step1Video">
                <video class="video" loop autoPlay={"autoplay"} muted src={video1} width="320" height="240" controls/>
              </div>
            </div>
            <div className="step2">
              <div className="step2Video">
                <video class="video" loop autoPlay={"autoplay"} muted src={video2} width="320" height="240" controls/>
              </div>
              <div className="step2Text">
                <h2>2. Book</h2>
                <p>Once you’ve found what you’re looking for, learn about your host, read past guest reviews, and get the details on
                cancellation options—then book in just a few clicks.</p>
              </div>
            </div>
            <div className="step3">
              <div className="step3Text">
                <h2>3. Go</h2>
                <p>You’re all set! Connect with your host through the app for local tips, questions or advice. You can also contact
                Airbnb Clone anytime for additional support.</p>
              </div>
              <div className="step3Video">
                <video class="video" loop autoPlay={"autoplay"} muted src={video3} width="320" height="240" controls/>
              </div>
            </div>
          </div>
        </main>

        <div className="location">
        </div>

        <Footer/>
      </div>
    </div>
  );
}

export default HomePage;