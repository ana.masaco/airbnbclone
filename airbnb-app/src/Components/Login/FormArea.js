import React from 'react';
import { FiArrowLeft } from 'react-icons/fi';
import { Link } from 'react-router-dom';

export default function FormLoginArea(props) {
  return (
    <div className="login-card">
      <div className="login-area">
        <form className="login-form">
          <fieldset>
            <legend>{props.title}</legend>
            {props.children}
          </fieldset>
          <button type="submit" className="login-button">
            Entrar
          </button>
        </form>
      </div>
      <Link to={props.url} className="button-back">
        <FiArrowLeft size={35} color="#15E098" />
      </Link>
    </div>
  );
}
